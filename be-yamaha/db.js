const Sequelize = require('sequelize');

const UsuarioModel = require('./models/usuario');
const VehiculoModel = require('./models/vehiculo');
const VentaModel = require('./models/venta');

const sequelize = new Sequelize('yamaha', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql'
});

const Usuario = UsuarioModel(sequelize, Sequelize);
const Vehiculo = VehiculoModel(sequelize, Sequelize);
const Venta = VentaModel(sequelize, Sequelize);

sequelize.sync({ force: false })
    .then(() => {
        console.log('Tablas creadas');
    })

module.exports = {
    Usuario,
    Vehiculo,
    Venta,
    sequelize,
}