const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const corsOpts = {
    origin: '*',
  
    methods: [
      'GET',
      'POST',
    ],
  
    allowedHeaders: [
      'Content-Type',
    ],
  };
app.use(cors(corsOpts));
require('./db');

const apiRouter = require('./routes/api');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api', apiRouter)

app.listen(3000, () => {
    console.log('la barracida lo mama');
});