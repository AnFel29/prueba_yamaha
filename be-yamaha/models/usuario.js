module.exports = (sequelize, type) => {
    return sequelize.define('usuarios', {
        id:{
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        cedula: type.STRING,
        nombres: type.STRING,
        apellidos: type.STRING,
        direccion: type.STRING,
        nacimiento: type.DATE,
        genero: type.STRING,
        celular: type.INTEGER,
        correo: {
            type: type.STRING,
            allowNull: false,
            unique: true,
            validate: {
              isEmail: {
                msg: "Must be a valid email address",
              }
            }
          },
    })
}