module.exports = (sequelize, type) => {
    return sequelize.define('vehiculos', {
        id:{
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        modelo: type.INTEGER,
        numero_motor: type.INTEGER,
        cilindraje: type.INTEGER,
        color: type.STRING,
        fecha_ensamble: type.DATE,
        ano_modelo: type.INTEGER,
    })
}