module.exports = (sequelize, type) => {
    return sequelize.define('ventas', {
        id:{
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        Fecha: type.DATE,
        numero_factura: type.INTEGER,
        Ciudad: type.STRING,
        Tienda: type.STRING,
        Precio: type.FLOAT,
        numero_moto: type.INTEGER,
        modelo: type.INTEGER,
        vendedor: type.INTEGER,
        usuario: type.INTEGER,
    })
}