const router = require('express').Router();

const apiVentasRouter = require('./api/ventas');

router.use('/ventas', apiVentasRouter);

module.exports = router;