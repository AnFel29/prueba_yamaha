const router = require('express').Router();
const { QueryTypes } = require('sequelize');
const { Venta, sequelize } = require('../../db');
const fs = require('fs');

router.get('/', async (req, res) => {
    const venta = await Venta.findAll();
    res.json(venta);
});

router.get('/promedio', async (req, res) => {
    // const ventas = await sequelize.query("SELECT * FROM `usuarios`", { type: QueryTypes.SELECT });
    res.json(ventas);
});

module.exports = router;