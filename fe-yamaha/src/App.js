import { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';
import axios from 'axios';

function App() {

  useEffect(() => {
    (async ()=>{
      const ventas = await axios.get('http://localhost:3000/api/ventas/');
      setVentas(ventas.data);
    })()
  },[])

  const [ventas, setVentas] = useState([]);
  console.log('ceee', ventas);
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>ciudad</th>
          <th>Fecha</th>
          <th>Precio</th>
          <th>Tienda</th>
        </tr>
      </thead>
      <tbody>
        {ventas.map(venta => (
          <tr>
            <td>{venta.id}</td>
            <td>{venta.Ciudad}</td>
            <td>{venta.Fecha}</td>
            <td>{venta.Precio}</td>
            <td>{venta.Tienda}</td>
        </tr>
        ))}
      </tbody>
    </Table>
  );
}

export default App;
